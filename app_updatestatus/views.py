from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from app_profile.views import nama, img_url


# Create your views here.
response = {}

def index(request):
    response['author'] = "Donatello"
    status = Status.objects.all()
    response['status'] = status
    html = 'update.html'
    response['status_form'] = Status_Form
    response['nama'] = nama
    response['img_url'] = img_url
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        response['isi'] = request.POST['isi']
        status = Status(isi = response['isi'])
        status.save()
        return HttpResponseRedirect('/update/')
    else:
        return HttpResponseRedirect('/update/')


