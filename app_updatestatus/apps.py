from django.apps import AppConfig


class AppUpdatestatusConfig(AppConfig):
    name = 'app_updatestatus'
