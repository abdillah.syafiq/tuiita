from django.db import models

# Create your models here.
class Status(models.Model):
	#database untuk menyimpan status dan tanggal updatenya 
    isi = models.CharField(max_length=140, default="...")
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
    	#mengurutkan query dengan terbaru duluan 
        ordering = ['-created_date']
