from django.shortcuts import render

# Create your views here.
nama = 'Donatello'
ulangTahun = '05 Oktober'
gender = 'Rather not say'
expertise = ['Kungfu','Fight','Eat Pizza']
desc = 'I am so smart turtle who likes to eat pizza. Feel free to visit me'
email = 'donnatelo@ninja.com'
img_url = "https://i.ebayimg.com/images/g/w58AAOSwhMpT0Rdq/s-l300.jpg"

def index(request):
	response={'name':nama,'birthDate':ulangTahun, 'gender':gender,'expertise':expertise,'description':desc,'email':email,
	'img_url' : img_url}
	return render(request,'profile.html',response)